package main

import (
	"fmt"
)

func main() {
	arr := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	fmt.Println(BinarySearch(arr, 1))
}

func BinarySearch(arr []int, val int) int {
	var half int
	limit := len(arr) - 1
	start := 0

	for start <= limit {
		half = (limit + start) / 2
		guess := arr[half]

		if guess == val {
			return half
		} else if val > guess {
			start = half + 1
		} else if val < guess {
			limit = half - 1
		}
	}

	return -1
}
