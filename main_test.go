package main

import (
	"testing"

	"github.com/go-playground/assert/v2"
)

func TestBinarySearch(t *testing.T) {
	testTable := []struct {
		name          string
		array         []int
		expectedIndex bool
		toFind        int
		expected      int
	}{
		{
			name:          "even array",
			array:         []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			expectedIndex: true,
		},
		{
			name:          "odd array",
			array:         []int{1, 2, 3, 4, 5, 6, 7, 8, 9},
			expectedIndex: true,
		},
		{
			name:          "not found more",
			array:         []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			expectedIndex: false,
			toFind:        20,
			expected:      -1,
		},
		{
			name:          "not found less",
			array:         []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			expectedIndex: false,
			toFind:        -5,
			expected:      -1,
		},
		{
			name:          "negative array OK",
			array:         []int{-5, -4, -3, -2, -1, 0},
			expectedIndex: false,
			toFind:        -4,
			expected:      1,
		},
		{
			name:          "negative array OK",
			array:         []int{-5, -4, -3, -2, -1, 0},
			expectedIndex: false,
			toFind:        0,
			expected:      5,
		},
		{
			name:          "negative array not found",
			array:         []int{-5, -4, -3, -2, -1, 0},
			expectedIndex: false,
			toFind:        -6,
			expected:      -1,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			if tt.expectedIndex {
				for i, v := range tt.array {
					find := BinarySearch(tt.array, v)
					assert.Equal(t, i, find)
				}
			} else {
				find := BinarySearch(tt.array, tt.toFind)
				assert.Equal(t, tt.expected, find)
			}
		})
	}
}
